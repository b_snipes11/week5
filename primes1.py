#First Prime Number Program
#Brady Russ
#10/1/15

def is_prime(num):

    if num < 2:
        print(num, "is not a valid integer. Please try again")
        return False
    else:
        #check all numbers 2 to one less than the number
        #when one of these goes into the number, return false
        for x in range(2, num):
            if(num % x == 0):
                return False

    return True

#-------------------------------------------

myNumber = 5791
for myNumber in range(1,101):
    if(is_prime(myNumber)):
        print(myNumber, "is a prime number")
    else:
        print(myNumber, "is not a prime number")
else:
    (is_prime(myNumber))
